import { ExchangeRateService } from './../services/exchange-rate.service';
import { DataService } from '../services/data.service';
import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  products: Product[] = [];

  constructor(
    private dataService: DataService,
    public exchangeRateService: ExchangeRateService
  ) {}

  ngOnInit(): void {
    this.loadData();
  }

  private loadData() {
    this.dataService.getProducts().subscribe((p) => (this.products = p));
  }
}
