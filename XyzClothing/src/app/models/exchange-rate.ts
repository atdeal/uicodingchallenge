export interface ExchangeRate {
  base: string;
  rates: { [base: string]: number };
}
