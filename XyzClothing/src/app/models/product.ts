import { ProductPrice } from './product-price';

export interface Product {
  id: number;
  name: string;
  price: ProductPrice;
  description: string;
  relatedProducts: number[];
}
