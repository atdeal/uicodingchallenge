export interface ProductPrice {
  base: string;
  amount: number;
}
