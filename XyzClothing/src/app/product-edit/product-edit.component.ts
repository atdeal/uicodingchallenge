import { ProductPrice } from './../models/product-price';
import { Product } from './../models/product';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { DataService } from '../services/data.service';
import { ExchangeRateService } from '../services/exchange-rate.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss'],
})
export class ProductEditComponent implements OnInit {
  id: number;
  product: Product;
  otherProducts: {
    id: number;
    name: string;
    price: ProductPrice;
    selected: boolean;
  }[];

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    public exchangeRateService: ExchangeRateService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.loadData(params);
    });
  }

  public save() {
    this.product.relatedProducts = this.otherProducts.filter(p => p.selected).map(p => p.id);

    this.dataService
      .updateProduct(this.id, this.product)
      .subscribe(() => this.back());
  }

  public back() {
    this.location.back();
  }

  private loadData(paramMap: ParamMap) {
    const id = paramMap.get('id');
    if (id) {
      this.id = +id;

      this.dataService.getProduct(+id).subscribe((p) => {
        this.product = p;

        this.dataService.getProducts().subscribe((r) => {
          this.otherProducts = r
            .filter((p) => p.id != this.id)
            .map((p) => ({
              id: p.id,
              name: p.name,
              price: p.price,
              selected: this.product.relatedProducts.includes(p.id),
            }));
        });
      });
    }
  }
}
