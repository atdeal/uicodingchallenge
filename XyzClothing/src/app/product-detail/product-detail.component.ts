import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Product } from '../models/product';
import { DataService } from '../services/data.service';
import { ExchangeRateService } from '../services/exchange-rate.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {
  product: Product;
  relatedProducts: Product[];

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    public exchangeRateService: ExchangeRateService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.loadData(params);
    });
  }

  private loadData(paramMap: ParamMap) {
    const id = paramMap.get('id');
    if (id) {
      this.dataService.getProduct(+id).subscribe((p) => {
        this.product = p;

        this.dataService
          .getRelatedProducts(this.product.relatedProducts)
          .subscribe((r) => (this.relatedProducts = r));
      });
    }
  }
}
