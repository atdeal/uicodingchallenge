import { ExchangeRateService } from './../services/exchange-rate.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
})
export class TopbarComponent implements OnInit {
  constructor(public exchangeRateService: ExchangeRateService) {
    exchangeRateService.initialise();
  }

  ngOnInit(): void {}
}
