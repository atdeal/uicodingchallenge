import { ExchangeRate } from './../models/exchange-rate';
import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  productsUrl = './assets/products.json';
  exchangeRatesUrl = './assets/exchange_rates.json';

  async createDb() {
    const result = await Promise.all([
      fetch(this.productsUrl),
      fetch(this.exchangeRatesUrl),
    ]);
    const mapResult = await Promise.all(
      result.map((x: { json: () => any }) => x.json())
    );
    const products = mapResult[0] as Product[];
    const exchangeRates = mapResult[1] as ExchangeRate[];
    return { products, exchangeRates };
  }

  genId(products: Product[]): number {
    return products.length > 0
      ? Math.max(...products.map((product) => product.id)) + 1
      : 11;
  }
}
