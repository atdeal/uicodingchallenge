import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Product } from '../models/product';
import { ExchangeRate } from '../models/exchange-rate';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private productsUrl = 'api/products';
  private exchangeRatesUrl = 'api/exchangeRates';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  exchangeRates: any[];

  constructor(private http: HttpClient) {}

  getProducts(): Observable<Product[]> {
    return this.http
      .get<Product[]>(this.productsUrl)
      .pipe(catchError(this.handleError<Product[]>('getProducts', [])));
  }

  getRelatedProducts(ids: number[]): Observable<Product[]> {
    var products = this.getProducts().pipe(
      map((data) => data.filter((p) => ids.includes(p.id)))
    );
    return products;
  }

  getProduct(id: number): Observable<Product> {
    const url = `${this.productsUrl}/${id}`;
    return this.http
      .get<Product>(url)
      .pipe(catchError(this.handleError<Product>(`getProduct id=${id}`)));
  }

  getExchangeRates(): Observable<ExchangeRate[]> {
    return this.http
      .get<ExchangeRate[]>(this.exchangeRatesUrl)
      .pipe(
        catchError(this.handleError<ExchangeRate[]>('getExchangeRates', []))
      );
  }

  updateProduct(id: number, product: Product): Observable<any> {
    const url = `${this.productsUrl}/${id}`;
    return this.http
      .put(url, product, this.httpOptions)
      .pipe(catchError(this.handleError<any>('updateProduct')));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
