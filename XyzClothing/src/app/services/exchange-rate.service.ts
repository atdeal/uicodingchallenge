import { DataService } from './data.service';
import { ExchangeRate } from './../models/exchange-rate';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ExchangeRateService {
  isInitialised: boolean;

  selectedRate: ExchangeRate;
  exchangeRates: ExchangeRate[];

  constructor(private dataService: DataService) {}

  public initialise() {
    if (this.isInitialised) return;

    this.dataService.getExchangeRates().subscribe((p) => {
      this.exchangeRates = p;
      this.selectedRate = p[1];
    });

    this.isInitialised = true;
  }
}
