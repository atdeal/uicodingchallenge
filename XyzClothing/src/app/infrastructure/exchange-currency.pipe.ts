import { Pipe, PipeTransform } from '@angular/core';
import { ExchangeRate } from '../models/exchange-rate';
import { ProductPrice } from '../models/product-price';

@Pipe({ name: 'exchangeCurrency' })
export class ExchangeCurrencyPipe implements PipeTransform {
  transform(value: ProductPrice, selectedRate: ExchangeRate): number {
    if (!selectedRate?.base) return 0;

    // get the base from the price, search it in the exchange rates table, convert to the selected rate
    if (value.base == selectedRate.base) return value.amount;

    var conversionFactor = selectedRate.rates[value.base];

    return value.amount / conversionFactor;
  }
}
