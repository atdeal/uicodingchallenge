# XyzClothing

Developed by Aaron Deal for Mudbath.

This is an Angular application used to display products from the XYZ Clothing catalog.

Data is provided by two json files in `src/assets/*.json` and are loaded into a mock in-memory backend.

## Development server

The [Angular CLI](https://github.com/angular/angular-cli) is required to build and run the application in a development environment.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
